package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.EpsEntity;

public interface EpsDAO extends CrudRepository<EpsEntity, Long> {
	
	public List<EpsEntity> findALLByStatusEquals(boolean decision);
	
	public Optional<EpsEntity> findOptionalByIdAndStatusEquals(Long id, boolean decision);
}
