package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.ArlEntity;

public interface ArlDAO extends CrudRepository<ArlEntity, Long> {
	
	public List<ArlEntity> findALLByStatusEquals(boolean decision);
	
	public Optional<ArlEntity> findOptionalByIdAndStatusEquals(Long id, boolean decision);
}
