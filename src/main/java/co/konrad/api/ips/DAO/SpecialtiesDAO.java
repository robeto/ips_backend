package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.SpecialtiesEntity;

public interface SpecialtiesDAO extends CrudRepository<SpecialtiesEntity, Long>{
	
	public List<SpecialtiesEntity> findAllByStatusEquals(boolean estatus);
	
	public Optional<SpecialtiesEntity> findOptionalByIdAndStatusEquals(Long id, boolean estatus);

}
