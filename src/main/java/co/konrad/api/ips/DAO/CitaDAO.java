package co.konrad.api.ips.DAO;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.CitaEntity;

public interface CitaDAO extends CrudRepository<CitaEntity, Long>{

}
