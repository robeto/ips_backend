package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.EspecialidadesTerapeutaEntity;

public interface EspecialidadesTerapeutaDAO extends CrudRepository<EspecialidadesTerapeutaEntity, Long> {
	
	public Optional<List<EspecialidadesTerapeutaEntity>> findAllOptionalByEspecialidad_id(Long id);
	
	public Optional<EspecialidadesTerapeutaEntity> findOptionalByTerapeuta_idAndEspecialidad_id(Long terapeuta, Long especialidad);
	
}
