package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.TerapeutaEntity;
import co.konrad.api.ips.entity.TypeIdEntity;

public interface TerapeutaDAO extends CrudRepository<TerapeutaEntity,Long>{
	
	public List<TerapeutaEntity> findALLByEstatusEquals(boolean decision);
	
	public Optional<TerapeutaEntity> findOptionalByIdAndEstatusEquals(Long id, boolean decision);
	
}
