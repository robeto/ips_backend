package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.TypeIdEntity;

public interface TypeIdDAO extends CrudRepository<TypeIdEntity, Long>{

	public List<TypeIdEntity> findALLByStatusEquals(boolean decision);
	
	public Optional<TypeIdEntity> findOptionalByIdAndStatusEquals(Long id, boolean decision);
	
}
