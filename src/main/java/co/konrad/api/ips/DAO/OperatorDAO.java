package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.OperatorEntity;

public interface OperatorDAO extends CrudRepository<OperatorEntity,Long>{
	
	public List<OperatorEntity> findALLByEstatusEquals(boolean decision);
	
	public Optional<OperatorEntity> findOptionalByIdAndEstatusEquals(Long id, boolean decision);
	
}
