package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import co.konrad.api.ips.entity.OrdenEntity;

public interface OrdenDAO extends CrudRepository<OrdenEntity, Long>{
	
	public List<OrdenEntity> findAllByPatient_numberIdAndEstatusEquals(String numberId, boolean estatus);
	
	public Optional<OrdenEntity> finOptionalByIdAndPatient_numberIdAndEstatusEquals(Long id, String numberId, boolean estatus);
}
