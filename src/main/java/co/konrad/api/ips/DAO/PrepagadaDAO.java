package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import co.konrad.api.ips.entity.PrepagadaEntity;

public interface PrepagadaDAO extends CrudRepository<PrepagadaEntity, Long> {
	
	public List<PrepagadaEntity> findALLByStatusEquals(boolean decision);
	
	public Optional<PrepagadaEntity> findOptionalByIdAndStatusEquals(Long id, boolean decision);
}
