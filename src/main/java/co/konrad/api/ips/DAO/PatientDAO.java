package co.konrad.api.ips.DAO;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;

import co.konrad.api.ips.entity.PatientEntity;

public interface PatientDAO extends PagingAndSortingRepository<PatientEntity,Long>{

	public PatientEntity findByNumberIdAndEstatusEquals(String numberId, boolean estatus);
	
	public List<PatientEntity> findALLByEstatusEquals(boolean decision);
	
	public Optional<PatientEntity> findOptionalByIdAndEstatusEquals(Long id, boolean decision);
}
