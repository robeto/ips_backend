package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.konrad.api.ips.DTO.OrdenDTO;
import co.konrad.api.ips.DTO.OrdenRequestDto;
import co.konrad.api.ips.entity.OrdenEntity;
import co.konrad.api.ips.service.OrdenService;


@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class OrdenController {
	
	@Autowired
	private OrdenService ordenService;

	@GetMapping("/orden-paciente/{numberId}")
	public ResponseEntity<List<OrdenDTO>> AllOrdensByPatient(@PathVariable String  numberId){
			return new ResponseEntity<List<OrdenDTO>>(ordenListEntityToListDTO(ordenService.getOrdenesPaciente(numberId)), HttpStatus.OK);
	}
		
	@PostMapping("/orden")
	public ResponseEntity<?> createOrden( @Valid @RequestBody OrdenDTO newOrden ){
		Map<String, Object> response = new HashMap<>();
		OrdenEntity orden = new OrdenEntity();
		try {
			BeanUtils.copyProperties(orden, newOrden);
			orden = ordenService.createOrden(orden);
			BeanUtils.copyProperties(orden, newOrden);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		
		response.put("mensajes: ", "La orden se ha credo exitosamente.");
		response.put("orden: ", newOrden);
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/orden-por-paciente")
	public ResponseEntity<?> getOrdenId(@RequestBody OrdenRequestDto ordenRequest){
		Map<String, Object> response = new HashMap<>();
		OrdenEntity orden = new OrdenEntity();
		OrdenDTO ordenDto = new OrdenDTO();
		try {
			orden = ordenService.findById(ordenRequest.getIdOrden(), ordenRequest.getNumberId());
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		if (orden == null) {
			response.put("mensaje", "La orden con Id: ".concat(ordenRequest.getIdOrden().toString().concat(" no ex�ste en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		BeanUtils.copyProperties(orden, ordenDto);
		return ResponseEntity.ok(ordenDto);
	}
	
	@PutMapping("/orden/{id}")
	public ResponseEntity<?> updateOrden(@RequestBody OrdenDTO orden, @PathVariable Long id) {
		OrdenEntity  ordenActual = new OrdenEntity();
		OrdenEntity update = new OrdenEntity();
		Map<String, Object> response = new HashMap<>();
		try{
			ordenActual = ordenService.getById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try{
			BeanUtils.copyProperties(orden, ordenActual);
			ordenActual.setId(id);
			update = ordenService.createOrden(ordenActual);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		BeanUtils.copyProperties(update, orden);
		response.put("orden: ", orden);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/orden/{id}")
	public ResponseEntity<?> deleteOrden(@PathVariable Long id) {
		OrdenEntity  ordenActual = new OrdenEntity();
		OrdenEntity update = new OrdenEntity();
		OrdenDTO orden = new OrdenDTO();
		Map<String, Object> response = new HashMap<>();
		try{
			ordenActual = ordenService.getById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try{
			BeanUtils.copyProperties(orden, ordenActual);
			ordenActual.setEstatus(false);
			update = ordenService.createOrden(ordenActual);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
		
	private static List<OrdenDTO> ordenListEntityToListDTO(List<OrdenEntity> ordenListEntity ){
		List<OrdenDTO> ordensDto = new ArrayList<>();
			for(OrdenEntity orden : ordenListEntity) {
				OrdenDTO auxDto = new OrdenDTO();
				BeanUtils.copyProperties(orden, auxDto);
				ordensDto.add(auxDto);
			}
		return ordensDto;
	}
	
}
