package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import co.konrad.api.ips.DTO.OperatorDTO;
import co.konrad.api.ips.entity.OperatorEntity;
import co.konrad.api.ips.service.OperatorService;

@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class OperatorController {
	
	@Autowired
	private OperatorService operatorService;
	
	@GetMapping("/operadores")
	public ResponseEntity<?> AllOperators(){
		return new ResponseEntity<>(operatorService.findAll(), HttpStatus.OK);
	}
	
	@PostMapping("/operadores")
	public ResponseEntity<?> createOperator( @Valid @RequestBody OperatorDTO newOperator ){
		Map<String, Object> response = new HashMap<>();
		OperatorEntity operador = new OperatorEntity();
		try {
			BeanUtils.copyProperties(newOperator, operador);
			operador = operatorService.save(operador);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensajes: ", "El operador se ha credo exitosamente.");
		BeanUtils.copyProperties(operador, newOperator);
		response.put("operador: ", newOperator);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/operadores/{id}")
	public ResponseEntity<?> getOperatorId(@PathVariable Long id){
		Map<String, Object> response = new HashMap<>();
		OperatorEntity operador = new OperatorEntity();
		OperatorDTO operatorDto = new OperatorDTO();
		try {
		operador = operatorService.findById(id);
		BeanUtils.copyProperties(operador, operatorDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		if (operador == null) {
			response.put("mensaje", "El operador con  Id: ".concat(id.toString().concat(" no ex�ste en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(operatorDto);
	}
	
	@PutMapping("/operadores/{id}")
	public ResponseEntity<?> updateOperator( @Valid @RequestBody OperatorDTO operator, @PathVariable Long id) {
		OperatorEntity actual = new OperatorEntity();
		OperatorEntity update = new OperatorEntity();
		Map<String, Object> response = new HashMap<>();
		try {
		BeanUtils.copyProperties(operator, actual);
		actual = operatorService.findById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try{
			update = operatorService.save(actual);
			BeanUtils.copyProperties(update, operator);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		response.put(" operador: ", operator);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/operadores/{id}")
	public ResponseEntity<?> deleteOperator(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		OperatorEntity actual = null;
		OperatorEntity update = null;
		OperatorDTO operadorDto = new OperatorDTO();
		try {
			actual = operatorService.findById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al buscar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		actual.setEstatus(false);
		try {
			update = operatorService.save(actual);
			BeanUtils.copyProperties(update, operadorDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
	
	private List<OperatorDTO> convertToLisDto(List<OperatorEntity> operadores){
		List<OperatorDTO> operadoresDto = new ArrayList<>();
		for(OperatorEntity operador : operadores) {
			OperatorDTO auxDto = new OperatorDTO();
			BeanUtils.copyProperties(operador, auxDto);
			operadoresDto.add(auxDto);
		}
		return operadoresDto;
	}
}
