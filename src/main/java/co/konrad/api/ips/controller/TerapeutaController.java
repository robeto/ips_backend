package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.konrad.api.ips.DTO.EspeacilidadTerapeutaRequestDto;
import co.konrad.api.ips.DTO.TerapeutaDTO;
import co.konrad.api.ips.entity.EspecialidadesTerapeutaEntity;
import co.konrad.api.ips.entity.SpecialtiesEntity;
import co.konrad.api.ips.entity.TerapeutaEntity;
import co.konrad.api.ips.service.EspecialidadesTerapeutaService;
import co.konrad.api.ips.service.SpecialtiesService;
import co.konrad.api.ips.service.TerapeutaService;

@CrossOrigin(origins = {"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class TerapeutaController {
	
	@Autowired
	private TerapeutaService terapeutaService;
	
	@Autowired
	private EspecialidadesTerapeutaService especialidadService;
	
	@Autowired
	private SpecialtiesService specialtiesService;
		
	@GetMapping("/terapeutas")
	public ResponseEntity<List<TerapeutaDTO>> AllTerapeutas(){
		return ResponseEntity.ok(terapeutaListEntityToListDTO(terapeutaService.findAll()));
	}
	
	@PostMapping("/terapeutas")
	public ResponseEntity<?> createTerapeuta( @Valid @RequestBody TerapeutaDTO newTerapeuta ){
		Map<String, Object> response = new HashMap<>();
		TerapeutaEntity terapeuta = new TerapeutaEntity();
		TerapeutaDTO terapeutaDto = new TerapeutaDTO();
		try {
			BeanUtils.copyProperties(newTerapeuta, terapeuta);
			terapeuta = terapeutaService.save(terapeuta);
			BeanUtils.copyProperties(terapeuta, terapeutaDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error no se puede hacer el insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.BAD_REQUEST);
		}
		response.put("mensaje", "Se creo con ex�to el terapeuta.");
		response.put("terapeuta", terapeutaDto);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/terapeutas/{id}")
	public ResponseEntity<?> getTerapeutaId(@PathVariable Long id){
		Map<String, Object> response = new HashMap<>();
		TerapeutaEntity terapeuta = null;
		TerapeutaDTO terapeutaDto = new TerapeutaDTO();
		try {
			terapeuta = terapeutaService.findById(id);
			BeanUtils.copyProperties(terapeuta, terapeutaDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al buscar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (terapeutaDto != null) {
			return ResponseEntity.ok(terapeutaDto);
		}
		response.put("mensaje", "No se encuentra el terapeuta por ID: ".concat(id.toString()).concat(" en la base de datos"));
		return  new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
	}
	
	@PutMapping("/terapeutas/{id}")
	@ResponseStatus(HttpStatus.CREATED)
	public ResponseEntity<?> updateTerpaeuta(@Valid @RequestBody TerapeutaDTO terapeuta, @PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		TerapeutaEntity actual = null;
		TerapeutaEntity update = null;
		TerapeutaDTO terapeutaDto = new TerapeutaDTO();
		try {
			actual = terapeutaService.findById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al buscar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		actual.setLastName(terapeuta.getLastName());
		actual.setAddress(terapeuta.getAddress());
		actual.setCellPhone(terapeuta.getCellPhone());
		actual.setName(terapeuta.getName());
		actual.setNumberId(terapeuta.getNumberId());
		actual.setPhone(terapeuta.getPhone());
		actual.setProfecionalTarjet(terapeuta.getProfecionalTarjet());
		actual.setTypeId(terapeuta.getTypeId());
		try {
			update = terapeutaService.save(actual);
			BeanUtils.copyProperties(update, terapeutaDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje", "Se actualizo con ex�to el terapeuta.");
		response.put("terapeuta", terapeutaDto);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/terapeutas/{id}")
	public ResponseEntity<?> deleteTerapeuta(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		TerapeutaEntity actual = null;
		TerapeutaEntity update = null;
		TerapeutaDTO terapeutaDto = new TerapeutaDTO();
		try {
			actual = terapeutaService.findById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al buscar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		actual.setEstatus(false);
		try {
			update = terapeutaService.save(actual);
			BeanUtils.copyProperties(update, terapeutaDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/crear-especialidad-terapeuta")
	public ResponseEntity<?> saveEspecialidadTerapeuta(@RequestBody EspeacilidadTerapeutaRequestDto especialidadRequest){
		Map<String, Object> response = new HashMap<>();
		TerapeutaEntity terapeutaEntity = new TerapeutaEntity();
		SpecialtiesEntity specialitiesEntity = new SpecialtiesEntity();
		EspecialidadesTerapeutaEntity newEspecialidad = new EspecialidadesTerapeutaEntity();
		try {
			TerapeutaEntity terapeuta = terapeutaService.findById(especialidadRequest.getIdTerapeuta());
			SpecialtiesEntity especialidad = specialtiesService.getById(especialidadRequest.getIdEspecialidad());
			BeanUtils.copyProperties(terapeuta, terapeutaEntity);
			BeanUtils.copyProperties(especialidad, specialitiesEntity);
			newEspecialidad = especialidadService.saveSpecialiti(terapeutaEntity, specialitiesEntity);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al crear la especialidad en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje", "Se creo exitosamente la especialidad para el terapeuta.");
		response.put("Especialidad", newEspecialidad);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.CREATED);
	}
	
	@PostMapping("/obtener-especialidad-terapeuta")
	public ResponseEntity<?> getTerapeutaByEspecilidad(@RequestBody EspeacilidadTerapeutaRequestDto especialidadRequest){
		Map<String, Object> response = new HashMap<>();
		EspecialidadesTerapeutaEntity especialidad = new EspecialidadesTerapeutaEntity();
		try {
			especialidad = especialidadService.findByTerpabeutaAndSpecialitie(especialidadRequest.getIdTerapeuta(), especialidadRequest.getIdEspecialidad());
		}catch (DataAccessException e) {
			response.put("mensaje", "No existe especialidad para este terapeuta");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		response.put("mensaje", "Se Encontro terapeuta con la especialidad.");
		response.put("Especialidad", especialidad);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	@PostMapping("/borrar-especialidad-terapeuta")
	public ResponseEntity<?> deleteTerapeutaByEspecilidad(@RequestBody EspeacilidadTerapeutaRequestDto especialidadRequest){
		Map<String, Object> response = new HashMap<>();
		EspecialidadesTerapeutaEntity especialidad = new EspecialidadesTerapeutaEntity();
		try {
			especialidad = especialidadService.findByTerpabeutaAndSpecialitie(especialidadRequest.getIdTerapeuta(), especialidadRequest.getIdEspecialidad());
			especialidad.setEstatus(false);
			especialidad = especialidadService.save(especialidad);
		}catch (DataAccessException e) {
			response.put("mensaje", "No existe especialidad para este terapeuta");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		response.put("mensaje", "Se borro la especialidad del terapeuta.");
		response.put("Especialidad", especialidad);
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.OK);
	}
	
	private List<TerapeutaDTO> terapeutaListEntityToListDTO(List<TerapeutaEntity> terapeutaListEntity ){
		List<TerapeutaDTO> terapeutasDto = new ArrayList<>();
			for(TerapeutaEntity terapeuta : terapeutaListEntity) {
				TerapeutaDTO auxDto = new TerapeutaDTO();
				BeanUtils.copyProperties(terapeuta, auxDto);
				terapeutasDto.add(auxDto);
			}
		return terapeutasDto;
	}
	
}
