package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.konrad.api.ips.DTO.ArlDTO;
import co.konrad.api.ips.entity.ArlEntity;
import co.konrad.api.ips.service.ArlService;

@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class ArlController {
	
	@Autowired
	private ArlService arlService;
	
	@GetMapping("/arl")
	public ResponseEntity<List<ArlDTO>> AllArl(){
		return ResponseEntity.ok(listEntityToListDTO(arlService.getAll()));
	}
	
	@PostMapping("/arl")
	public ResponseEntity<?> createArl( @Valid @RequestBody ArlDTO newArl ){
		ArlEntity arl = new ArlEntity();
		Map<String, Object> response = new HashMap<>();
		try {
			BeanUtils.copyProperties(newArl, arl);
			arl = arlService.newArl(arl);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensajes: ", "La ARL se ha credo exitosamente.");
		BeanUtils.copyProperties(arl, newArl);
		response.put("type id: ", newArl);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/arl/{id}")
	public ResponseEntity<?> getArl(@PathVariable Long id){
		ArlEntity arl = new ArlEntity();
		ArlDTO arlDto = new ArlDTO();
		Map<String, Object> response = new HashMap<>();
		try {
			arl = arlService.getArl(id);
			BeanUtils.copyProperties(arl, arlDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		return ResponseEntity.ok(arlDto);
		
	}
	
	@PutMapping("/arl/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody ArlDTO arlDto, @PathVariable Long id) {
			ArlEntity actual = new ArlEntity();
			ArlEntity update = new ArlEntity();
			Map<String, Object> response = new HashMap<>();
		try {
			actual = arlService.getArl(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try {
			actual.setName(arlDto.getName());
			update = arlService.newArl(actual);
			BeanUtils.copyProperties(update, arlDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		response.put("ARL: ", arlDto);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/arl/{id}")
	public ResponseEntity<?> delete (@PathVariable Long id) {
		ArlEntity actual = new ArlEntity();
		ArlEntity update = new ArlEntity();
		ArlDTO arlDto = new ArlDTO();
		Map<String, Object> response = new HashMap<>();
	try {
		actual = arlService.getArl(id);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al realizar consulta en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
	}
	try {
		actual.setStatus(false);
		update = arlService.newArl(actual);
		BeanUtils.copyProperties(update, arlDto);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al Borrar en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
	}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
	
	private List<ArlDTO> listEntityToListDTO(List<ArlEntity> arlListEntity ){
		List<ArlDTO> arlDtos = new ArrayList<>();
			for(ArlEntity arl : arlListEntity) {
				ArlDTO auxDto = new ArlDTO();
				BeanUtils.copyProperties(arl, auxDto);
				arlDtos.add(auxDto);
				
			}
		return arlDtos;
	}
}
