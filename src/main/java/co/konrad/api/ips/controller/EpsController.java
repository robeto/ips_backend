package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.konrad.api.ips.DTO.EpsDTO;
import co.konrad.api.ips.entity.EpsEntity;
import co.konrad.api.ips.service.EpsService;

@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class EpsController {
	
	@Autowired
	private EpsService epsService;
	
	@GetMapping("/eps")
	public ResponseEntity<List<EpsDTO>> AllEps(){
		return ResponseEntity.ok(listEntityToListDTO(epsService.getAll()));
	}
	
	@PostMapping("/eps")
	public ResponseEntity<?> createEps( @Valid @RequestBody EpsDTO newEps ){
		EpsEntity eps = new EpsEntity();
		Map<String, Object> response = new HashMap<>();
		try {
			BeanUtils.copyProperties(newEps, eps);
			eps = epsService.newEps(eps);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensajes: ", "La EPS se ha credo exitosamente.");
		BeanUtils.copyProperties(eps, newEps);
		response.put("EPS: ", newEps);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/eps/{id}")
	public ResponseEntity<?> getEps(@PathVariable Long id){
		EpsEntity eps = new EpsEntity();
		EpsDTO epsDto = new EpsDTO();
		Map<String, Object> response = new HashMap<>();
		try {
			eps = epsService.getEps(id);
			BeanUtils.copyProperties(eps, epsDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		return ResponseEntity.ok(epsDto);
		
	}
	
	@PutMapping("/eps/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody EpsDTO epsDto, @PathVariable Long id) {
			EpsEntity actual = new EpsEntity();
			EpsEntity update = new EpsEntity();
			Map<String, Object> response = new HashMap<>();
		try {
			actual = epsService.getEps(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try {
			actual.setName(epsDto.getName());
			update = epsService.newEps(actual);
			BeanUtils.copyProperties(update, epsDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		response.put("EPS: ", epsDto);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/eps/{id}")
	public ResponseEntity<?> delete (@PathVariable Long id) {
		EpsEntity actual = new EpsEntity();
		EpsEntity update = new EpsEntity();
		EpsDTO epsDto = new EpsDTO();
		Map<String, Object> response = new HashMap<>();
	try {
		actual = epsService.getEps(id);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al realizar consulta en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
	}
	try {
		actual.setStatus(false);
		update = epsService.newEps(actual);
		BeanUtils.copyProperties(update, epsDto);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al Borrar en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
	}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
	
	private List<EpsDTO> listEntityToListDTO(List<EpsEntity> epsListEntity ){
		List<EpsDTO> epsDtos = new ArrayList<>();
			for(EpsEntity eps : epsListEntity) {
				EpsDTO auxDto = new EpsDTO();
				BeanUtils.copyProperties(eps, auxDto);
				epsDtos.add(auxDto);
				
			}
		return epsDtos;
	}

}
