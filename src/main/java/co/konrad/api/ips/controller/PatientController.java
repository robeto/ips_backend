package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.konrad.api.ips.DTO.PatientDTO;
import co.konrad.api.ips.entity.PatientEntity;
import co.konrad.api.ips.service.PatientService;

@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class PatientController {
	
	@Autowired
	private PatientService patientService;
		
	@GetMapping("/pacientes")
	public ResponseEntity<List<PatientDTO>> AllPatients(){
		return ResponseEntity.ok(patientListEntityToListDTO(patientService.findAll()));
	}
	
	@PostMapping("/pacientes")
	public ResponseEntity<?> createPatient( @Valid @RequestBody PatientDTO newpatient ){
		PatientEntity paciente = new PatientEntity();
		Map<String, Object> response = new HashMap<>();
		try {
			BeanUtils.copyProperties(newpatient, paciente);
			paciente = patientService.save(paciente);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensajes: ", "El tipo id se ha credo exitosamente.");
		BeanUtils.copyProperties(paciente, newpatient);
		response.put("paciente: ", newpatient);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/pacientes/{id}")
	public ResponseEntity<?> getPatientId(@PathVariable Long id){
		PatientEntity paciente = new PatientEntity();
		PatientDTO pacienteDto = new PatientDTO();
		Map<String, Object> response = new HashMap<>();
		try {
			paciente = patientService.findById(id);
			BeanUtils.copyProperties(paciente, pacienteDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		if (pacienteDto == null) {
			response.put("mensaje", "El paciente con Id: ".concat(id.toString().concat(" no ex�ste en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(pacienteDto);
	}
	
	@GetMapping("/paciente/{number}")
	public ResponseEntity<?> getPatientNumberId(@PathVariable String number){
		PatientEntity paciente = new PatientEntity();
		PatientDTO pacienteDto = new PatientDTO();
		Map<String, Object> response = new HashMap<>();
		try {
			paciente = patientService.findByNumberId(number);
			BeanUtils.copyProperties(paciente, pacienteDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		if (pacienteDto == null) {
			response.put("mensaje", "El paciente con numero de cedula: ".concat(number.toString().concat(" no ex�ste en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(pacienteDto);
	}
	
	@PutMapping("/pacientes/{id}")
	public ResponseEntity<?> updatePatient(@Valid @RequestBody PatientDTO paciente, @PathVariable Long id) {
		PatientEntity actual = new PatientEntity();
		PatientEntity update = new PatientEntity();
		Map<String, Object> response = new HashMap<>();
		try{
			BeanUtils.copyProperties(paciente, actual);
			actual.setId(id);
			update = patientService.save(actual);
			BeanUtils.copyProperties(actual, paciente);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		response.put("paciente: ", paciente);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/pacientes/{id}")
	public ResponseEntity<?> deletePatient(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		PatientEntity paciente = new PatientEntity();
		try {
			paciente = patientService.findById(id);
			paciente.setEstatus(false);
			paciente = patientService.save(paciente);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al borrar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
	
	private static List<PatientDTO> patientListEntityToListDTO(List<PatientEntity> patientListEntity ){
		List<PatientDTO> patientsDto = new ArrayList<>();
		for(PatientEntity paciente : patientListEntity) {
			PatientDTO auxDto = new PatientDTO();
			BeanUtils.copyProperties(paciente, auxDto);
			patientsDto.add(auxDto);
		}
		return patientsDto;
	}
	
}
