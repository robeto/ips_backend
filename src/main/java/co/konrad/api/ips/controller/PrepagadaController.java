package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.konrad.api.ips.DTO.PrepagadaDTO;
import co.konrad.api.ips.entity.PrepagadaEntity;
import co.konrad.api.ips.service.PrepagadaService;

@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class PrepagadaController {
	
	@Autowired
	private PrepagadaService prepagadaService;
	
	@GetMapping("/prepagada")
	public ResponseEntity<List<PrepagadaDTO>> AllPrepagada(){
		return ResponseEntity.ok(listEntityToListDTO(prepagadaService.getAll()));
	}
	
	@PostMapping("/prepagada")
	public ResponseEntity<?> createPrepagada( @Valid @RequestBody PrepagadaDTO newPrepagada){
		PrepagadaEntity prepagada = new PrepagadaEntity();
		Map<String, Object> response = new HashMap<>();
		try {
			BeanUtils.copyProperties(newPrepagada, prepagada);
			prepagada = prepagadaService.newPrepagada(prepagada);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensajes: ", "La medicina prepagada se ha credo exitosamente.");
		BeanUtils.copyProperties(prepagada, newPrepagada);
		response.put("type id: ", newPrepagada);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/prepagada/{id}")
	public ResponseEntity<?> getPrepagada(@PathVariable Long id){
		PrepagadaEntity prepagada = new PrepagadaEntity();
		PrepagadaDTO prepagadaDto = new PrepagadaDTO();
		Map<String, Object> response = new HashMap<>();
		try {
			prepagada = prepagadaService.getPrepagada(id);
			BeanUtils.copyProperties(prepagada, prepagadaDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		return ResponseEntity.ok(prepagadaDto);
		
	}
	
	@PutMapping("/prepagada/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody PrepagadaDTO prepagadaDto, @PathVariable Long id) {
		PrepagadaEntity actual = new PrepagadaEntity();
		PrepagadaEntity update = new PrepagadaEntity();
			Map<String, Object> response = new HashMap<>();
		try {
			actual = prepagadaService.getPrepagada(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try {
			actual.setName(prepagadaDto.getName());
			update = prepagadaService.newPrepagada(actual);
			BeanUtils.copyProperties(update, prepagadaDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		response.put("Prepagada: ", prepagadaDto);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/prepagada/{id}")
	public ResponseEntity<?> delete (@PathVariable Long id) {
		PrepagadaEntity actual = new PrepagadaEntity();
		PrepagadaEntity update = new PrepagadaEntity();
		PrepagadaDTO prepagadaDto = new PrepagadaDTO();
		Map<String, Object> response = new HashMap<>();
	try {
		actual = prepagadaService.getPrepagada(id);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al realizar consulta en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
	}
	try {
		actual.setStatus(false);
		update = prepagadaService.newPrepagada(actual);
		BeanUtils.copyProperties(update, prepagadaDto);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al Borrar en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
	}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
	
	private List<PrepagadaDTO> listEntityToListDTO(List<PrepagadaEntity> prepagadaListEntity ){
		List<PrepagadaDTO> prepagadaDtos = new ArrayList<>();
			for(PrepagadaEntity prepagada : prepagadaListEntity) {
				PrepagadaDTO auxDto = new PrepagadaDTO();
				BeanUtils.copyProperties(prepagada, auxDto);
				prepagadaDtos.add(auxDto);
				
			}
		return prepagadaDtos;
	}
	
}
