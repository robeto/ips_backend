package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.konrad.api.ips.DTO.CitaDTO;
import co.konrad.api.ips.DTO.TypeIdDTO;
import co.konrad.api.ips.entity.CitaEntity;
import co.konrad.api.ips.entity.TypeIdEntity;
import co.konrad.api.ips.service.CitaService;

@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class CitaController {
	
	@Autowired
	private CitaService citaService;
		
	@GetMapping("/citas")
	public ResponseEntity<List<CitaDTO>> AllCitas(){
		return ResponseEntity.ok(citaListEntityToListDTO(citaService.findAll()));
	}
	
	@PostMapping("/citas")
	public ResponseEntity<?> createCita( @Valid @RequestBody CitaDTO newcita ){
		CitaEntity cita = null;
		Map<String, Object> response = new HashMap<>();
		try {
		cita = citaService.save(citaDtoToEntity(newcita));
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensajes: ", "La cita se ha credo exitosamente.");
		response.put(" cita: ", cita);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/citas/{id}")
	public ResponseEntity<?> getCitaId(@PathVariable Long id){
		CitaEntity cita = null;
		Map<String, Object> response = new HashMap<>();
		try {
			cita = citaService.findById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (cita == null) {
			response.put("mensaje", "La cita con Id: ".concat(id.toString().concat(" no ex�ste en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(cita);
	}
	
	@PutMapping("/citas/{id}")
	public ResponseEntity<?> updateCita( @Valid @RequestBody CitaDTO cita, @PathVariable Long id) {
		CitaEntity actual = null;
		CitaEntity update = null;
		Map<String, Object> response = new HashMap<>();
	try {
		actual = citaService.findById(id);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al realizar consulta en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	try {
			actual = citaDtoToEntity(cita);
			actual.setId(id);
			update = citaService.save(actual);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		response.put("cita: ", update);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/citas/{id}")
	public ResponseEntity<?> deleteCita(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		try {
			citaService.delete(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al borrar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
	
	private static CitaEntity citaDtoToEntity(CitaDTO p) {
		return new CitaEntity(p.getId(), p.getHorarioTerapeuta(), p.getTerapeutas(), p.getZona(), p.getCreateAt(),
				p.getEvolucion(), p.getObservaciones(), p.getPago(), p.getOrden());
	}
	
	private static CitaDTO citaEntiyToDTO(CitaEntity p) {
		return new CitaDTO(p.getId(), p.getHorarioTerapeuta(),p.getTerapeutas(), p.getZona(), p.getCreateAt(), p.getEvolucion(),p.getObservaciones(),
				p.getPago(), p.getOrden()); 
	}
	
	private static List<CitaDTO> citaListEntityToListDTO(List<CitaEntity> citaListEntity ){
		List<CitaDTO> citaDto = new ArrayList<>();
		CitaDTO auxDto = new CitaDTO();
			for(CitaEntity cita : citaListEntity) {
				auxDto = citaEntiyToDTO(cita);
				citaDto.add(auxDto);
			}
		return citaDto;
	}
	
}
