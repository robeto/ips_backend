package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import co.konrad.api.ips.DTO.TypeIdDTO;
import co.konrad.api.ips.entity.TypeIdEntity;
import co.konrad.api.ips.service.TypeIdService;

@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class TypeIdController {
	
	@Autowired
	private TypeIdService typeIdService;
	
	@GetMapping("/type-id")
	public ResponseEntity<List<TypeIdDTO>> AllTypeId(){
		return ResponseEntity.ok(typeIdListEntityToListDTO(typeIdService.getAll()));
	}
	
	@PostMapping("/type-id")
	public ResponseEntity<?> createTypeId( @Valid @RequestBody TypeIdDTO newTypeId ){
		TypeIdEntity typeId = new TypeIdEntity();
		Map<String, Object> response = new HashMap<>();
		try {
			BeanUtils.copyProperties(newTypeId, typeId);
			typeId = typeIdService.newTypeId(typeId);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensajes: ", "El tipo id se ha credo exitosamente.");
		response.put("type id: ", typeId);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/type-id/{id}")
	public ResponseEntity<?> getTypeId(@PathVariable Long id){
		TypeIdDTO typeId = new TypeIdDTO();
		Map<String, Object> response = new HashMap<>();
		try {
			BeanUtils.copyProperties(typeIdService.getTypeId(id), typeId);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if (typeId == null) {
			response.put("mensaje", "El cliente Id: ".concat(id.toString().concat(" no ex�ste en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(typeId);
		
	}
	
	@PutMapping("/type-id/{id}")
	public ResponseEntity<?> update(@Valid @RequestBody TypeIdDTO typeId, @PathVariable Long id) {
			TypeIdEntity actual = null;
			TypeIdEntity update = null;
			Map<String, Object> response = new HashMap<>();
		try {
			actual = typeIdService.getTypeId(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		try {
			actual.setName(typeId.getName());
			update = typeIdService.newTypeId(actual);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		response.put("type id: ", update);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/type-id/{id}")
	public ResponseEntity<?> delete (@PathVariable Long id) {
		TypeIdEntity actual = null;
		TypeIdEntity update = null;
		Map<String, Object> response = new HashMap<>();
	try {
		actual = typeIdService.getTypeId(id);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al realizar consulta en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	try {
		actual.setStatus(false);
		update = typeIdService.newTypeId(actual);
	}catch (DataAccessException e) {
		response.put("mensaje", "Error al Borrar en la base de datos.");
		response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
		return new ResponseEntity<Map<String, Object>>(response, HttpStatus.INTERNAL_SERVER_ERROR);
	}
		response.put("mensaje", "Se borro con ex�to.");
		return ResponseEntity.ok(response);
	}
	
	private List<TypeIdDTO> typeIdListEntityToListDTO(List<TypeIdEntity> typeIdListEntity ){
		List<TypeIdDTO> typeIdDto = new ArrayList<>();
			for(TypeIdEntity typeIdite : typeIdListEntity) {
				TypeIdDTO auxDto = new TypeIdDTO();
				BeanUtils.copyProperties(typeIdite, auxDto);
				typeIdDto.add(auxDto);
				
			}
		return typeIdDto;
	}

}
