package co.konrad.api.ips.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.konrad.api.ips.DTO.SpecialtiesDTO;
import co.konrad.api.ips.entity.SpecialtiesEntity;
import co.konrad.api.ips.service.SpecialtiesService;

@CrossOrigin(origins ={"http://localhost:8100"})
@RestController
@RequestMapping("/api")
public class SpecialtiesController {
	
	@Autowired
	private SpecialtiesService specialtiesService;
		
	@GetMapping("/specialties")
	public ResponseEntity<List<SpecialtiesDTO>> AllSpecialties(){
		return ResponseEntity.ok(ListEntityToListDTO(specialtiesService.getAll()));
	}
	
	@PostMapping("/specialties")
	public ResponseEntity<?> createSpecialties( @Valid @RequestBody SpecialtiesDTO newSpecialties ){
		Map<String, Object> response = new HashMap<>();
		SpecialtiesEntity especialidad = new SpecialtiesEntity();
		try {
			BeanUtils.copyProperties(newSpecialties, especialidad);
			especialidad = specialtiesService.save(especialidad);
			BeanUtils.copyProperties(especialidad, newSpecialties);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar insert en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		response.put("mensajes: ", "La especialidad se ha credo exitosamente.");
		response.put("especialidad: ", newSpecialties);
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/specialties/{id}")
	public ResponseEntity<?> getSpecialtiesId(@PathVariable Long id){
		Map<String, Object> response = new HashMap<>();
		SpecialtiesEntity especialidad = new SpecialtiesEntity();
		SpecialtiesDTO especialidadDto = new SpecialtiesDTO()
;		try {
			especialidad = specialtiesService.getById(id);
			BeanUtils.copyProperties(especialidad, especialidadDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		
		if (especialidad == null) {
			response.put("mensaje", "El cliente Id: ".concat(id.toString().concat(" no ex�ste en la base de datos.")));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(especialidadDto);
	}
	
	@PutMapping("/specialties/{id}")
	public ResponseEntity<?> updateSpecialties(@Valid @RequestBody SpecialtiesDTO specialties, @PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		SpecialtiesEntity actual = null;
		SpecialtiesEntity update = null;
		try {
			actual = specialtiesService.getById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try {
			actual.setName(specialties.getName());
			update = specialtiesService.save(actual);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		
		response.put("mensaje: ", "Actaulizaci�n exitosa.");
		BeanUtils.copyProperties(update, specialties);
		response.put("type id: ", specialties);
		return  ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/specialties/{id}")
	public ResponseEntity<?> deleteSpecialties(@PathVariable Long id) {
		Map<String, Object> response = new HashMap<>();
		SpecialtiesEntity actual = new SpecialtiesEntity();
		SpecialtiesEntity update = new SpecialtiesEntity();
		SpecialtiesDTO especialidadDto = new SpecialtiesDTO();
		try {
			actual = specialtiesService.getById(id);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al realizar consulta en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		try {
			actual.setStatus(false);
			update = specialtiesService.save(actual);
			BeanUtils.copyProperties(update, especialidadDto);
		}catch (DataAccessException e) {
			response.put("mensaje", "Error al actualizar en la base de datos.");
			response.put("error", e.getMessage().concat(": ").concat(e.getMostSpecificCause().getMessage()));
			return new ResponseEntity<Map<String, Object>>(response, HttpStatus.NO_CONTENT);
		}
		
		response.put("mensaje: ", "Borrado exitoso.");
		response.put("type id: ", especialidadDto);
		return  ResponseEntity.ok(response);
	}
	
	private List<SpecialtiesDTO> ListEntityToListDTO(List<SpecialtiesEntity> SpecialtiesListEntity ){
		List<SpecialtiesDTO> specialtiesDTO = new ArrayList<>();
			for(SpecialtiesEntity specialtiesIte : SpecialtiesListEntity) {
				SpecialtiesDTO auxDto = new SpecialtiesDTO();
				BeanUtils.copyProperties(specialtiesIte, auxDto);
				specialtiesDTO.add(auxDto);
				
			}
		return specialtiesDTO;
	}
	
}
