package co.konrad.api.ips.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class indexController {
	
	@GetMapping("/")
	public String index() {
		return "Dirigete a https://api-ips.herokuapp.com/swagger-ui.html para conocer como funciona la api-ips";
	}
}
