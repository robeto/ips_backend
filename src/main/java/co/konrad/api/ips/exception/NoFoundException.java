package co.konrad.api.ips.exception;

public class NoFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public NoFoundException(String msg) {
		super(msg);
	}

}
