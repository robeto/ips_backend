package co.konrad.api.ips.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorInfo {

     
	@JsonProperty("status_code")
	 private Integer httpStatus;
	 
	@JsonProperty("message")
	 private String message;
	
	@JsonProperty("uri")
	 private String uri;
	 
	public ErrorInfo(Integer httpStatus, String message, String uri) {
		this.httpStatus = httpStatus;
		this.message = message;
		this.uri = uri;
	}
	
	public ErrorInfo(Exception exception, String uriRequested) {
	       this.message = exception.getMessage();
	       this.httpStatus = 400;
	       this.uri = uriRequested;
	   }

	public ErrorInfo() {}

	public Integer getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(Integer httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

}
