package co.konrad.api.ips.util;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import co.konrad.api.ips.exception.NoFoundException;

@ControllerAdvice
public class ErrorHandler {
	
	@ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<ErrorInfo> methodArgumentNotValidException(HttpServletRequest request, ConstraintViolationException e) {
		ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST.value(), e.getMessage(), request.getRequestURI());
	       return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(NoFoundException.class)
    public ResponseEntity<ErrorInfo> methodArgumentNotValidException(HttpServletRequest request, NoFoundException e) {
		ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST.value(), e.getMessage(), request.getRequestURI());
	       return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
    }
	
	@ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ErrorInfo> methodArgumentNotValidException(HttpServletRequest request, EntityNotFoundException e) {
		ErrorInfo errorInfo = new ErrorInfo(HttpStatus.BAD_REQUEST.value(), e.getMessage(), request.getRequestURI());
	       return new ResponseEntity<>(errorInfo, HttpStatus.BAD_REQUEST);
    }

}
