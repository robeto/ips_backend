package co.konrad.api.ips.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "paciente")
public class PatientEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@NotNull(message = "Debe ingresar un tipo de id existente.")
	@OneToOne
	@JoinColumn(name = "type_id")
	private TypeIdEntity typeId;
	
	@Pattern(regexp = "[0-9]{8,10}+", message = "Debe ingresar de 8 a 10 valores numericos en el campo numberId.")
	@Column(name = "numero_id")
	private String numberId;
	
	@Pattern(regexp = "^\\D{3,40}+$", message = "El nombre solo puede tener de 3 a 40 letras.")
	private String name;
	
	@Pattern(regexp = "^\\D{3,40}+$", message = "Solo puede tener de 3 a 40 letras el apellido")
	@Column(name = "last_name")
	private String lastName;
	
	@Pattern(regexp = "[0-9]{7}+", message = "Solo puede tener maximo 7 numeros.")
	private String phone;
	
	@Pattern(regexp = "[0-9]{10}+", message = "Solo puede tener maximo 10 numeros.")
	@Column(name = "cellphone")
	private String cellPhone;
	
	@Size(min = 8, max = 40, message="Debe tener minimo 8 caracteres y maximo 40.")
	private String address;
	
	@NotNull(message = "Debe ingresar una EPS existente.")
	@OneToOne
	@JoinColumn(name = "eps")
	private EpsEntity eps;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;
	
	@JsonProperty
	@Column(name = "is_prepagada")
	private boolean isPrepagada;
	
	@NotNull(message = "Debe ingresar un tipo de prepagada existente.")
	@OneToOne
	@JoinColumn(name = "prepagada")
	private PrepagadaEntity prepagada;
	
	@NotNull(message = "Debe ingresar una arl existente.")
	@OneToOne
	@JoinColumn(name = "arl")
	private ArlEntity arl;
	
	@JsonProperty
	@Column(name = "is_child")
	private boolean isChild;
	
	@JsonProperty
	@Column(name = "is_old")
	private boolean isOld;
	
	@Pattern(regexp = "^\\D{3,40}+$", message = "Solo puede tener de 3 a 40 letras el acudiente")
	private String acudiente;
	
	@Pattern(regexp = "[0-9]{10}+", message = "Solo puede tener maximo 10 numeros.")
	@Column(name = "phone_acudiente")
	private String phoneAcudiente;
	
	@JsonIgnore
	@Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;
	
	private boolean estatus;
	
	public PatientEntity() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeIdEntity getTypeId() {
		return typeId;
	}

	public void setTypeId(TypeIdEntity typeId) {
		this.typeId = typeId;
	}

	public String getNumberId() {
		return numberId;
	}

	public void setNumberId(String numberId) {
		this.numberId = numberId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public EpsEntity getEps() {
		return eps;
	}

	public void setEps(EpsEntity eps) {
		this.eps = eps;
	}

	public boolean isPrepagada() {
		return isPrepagada;
	}

	public void setPrepagada(boolean isPrepagada) {
		this.isPrepagada = isPrepagada;
	}

	public boolean isChild() {
		return isChild;
	}

	public void setChild(boolean isChild) {
		this.isChild = isChild;
	}

	public boolean isOld() {
		return isOld;
	}

	public void setOld(boolean isOld) {
		this.isOld = isOld;
	}

	public String getAcudiente() {
		return acudiente;
	}

	public void setAcudiente(String acudiente) {
		this.acudiente = acudiente;
	}

	public String getPhoneAcudiente() {
		return phoneAcudiente;
	}

	public void setPhoneAcudiente(String phoneAcudiente) {
		this.phoneAcudiente = phoneAcudiente;
	}

	public Date getCreateAt() {
		return createAt;
	}
	

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	@PrePersist
	public void initial() {
		this.createAt = new Date();
		this.estatus = true;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public PrepagadaEntity getPrepagada() {
		return prepagada;
	}

	public void setPrepagada(PrepagadaEntity prepagada) {
		this.prepagada = prepagada;
	}

	public ArlEntity getArl() {
		return arl;
	}

	public void setArl(ArlEntity arl) {
		this.arl = arl;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

}
