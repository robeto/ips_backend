package co.konrad.api.ips.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "citas")
public class CitaEntity implements Serializable{

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "horario_terapeuta")
	private Date horarioTerapeuta;
	
	@JoinColumn(name="terpaeuta_id")
	@OneToOne
	private TerapeutaEntity terapeutas;
	
	@JsonProperty
	private String zona;
	
	@Column(name="create_at")
	@JsonIgnore
	private String createAt;
	
	@JsonProperty
	private String evolucion;
	
	@JsonProperty
	private String observaciones;
	
	@JsonProperty
	private String pago;
	
	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	@JsonBackReference
	private OrdenEntity orden;
	
	public CitaEntity() {}
	
	public CitaEntity(Long id, Date horarioTerapeuta,TerapeutaEntity terapeutas,
			String zona, String createAt, String evolucion, String observaciones,
			String pago, OrdenEntity orden) {
		super();
		this.id = id;
		this.horarioTerapeuta = horarioTerapeuta;
		this.terapeutas = terapeutas;
		this.zona = zona;
		this.createAt = createAt;
		this.evolucion = evolucion;
		this.observaciones = observaciones;
		this.pago = pago;
		this.orden = orden;
	}
	
	
	
	public OrdenEntity getOrden() {
		return orden;
	}

	public void setOrden(OrdenEntity orden) {
		this.orden = orden;
	}

	public TerapeutaEntity getTerapeutas() {
		return terapeutas;
	}

	public void setTerapeutas(TerapeutaEntity terapeutas) {
		this.terapeutas = terapeutas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getHorarioTerapeuta() {
		return horarioTerapeuta;
	}

	public void setHorarioTerapeuta(Date horarioTerapeuta) {
		this.horarioTerapeuta = horarioTerapeuta;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getCreateAt() {
		return createAt;
	}

	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}

	public String getEvolucion() {
		return evolucion;
	}

	public void setEvolucion(String evolucion) {
		this.evolucion = evolucion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPago() {
		return pago;
	}

	public void setPago(String pago) {
		this.pago = pago;
	}
	
}
