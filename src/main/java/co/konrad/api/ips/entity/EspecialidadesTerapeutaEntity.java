package co.konrad.api.ips.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "especialidades_terapeuta")
@JsonIgnoreProperties(value={"specialties", "hibernateLazyInitializer", "handler"}, allowSetters=true)
public class EspecialidadesTerapeutaEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private TerapeutaEntity terapeuta;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private SpecialtiesEntity especialidad;
	
	@JsonIgnore
	@Column(name="create_at")
	private Date createAt;
	
	@JsonIgnore
	private boolean estatus;
	
	public EspecialidadesTerapeutaEntity() {}

	public EspecialidadesTerapeutaEntity(TerapeutaEntity terapeuta, SpecialtiesEntity especialidad) {
		this.terapeuta = terapeuta;
		this.especialidad = especialidad;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TerapeutaEntity getTerapeuta() {
		return terapeuta;
	}

	public void setTerapeuta(TerapeutaEntity terapeuta) {
		this.terapeuta = terapeuta;
	}

	public SpecialtiesEntity getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(SpecialtiesEntity especialidad) {
		this.especialidad = especialidad;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}
	
	@PrePersist
	public void initialDate() {
		this.estatus = true;
		this.createAt = new Date();
	}

}
