package co.konrad.api.ips.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "oredenes")
public class OrdenEntity implements Serializable{
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@JsonIgnore
	private Long id;
	
	@Column(name = "cantidad_citas")
	private Integer cantidadCitas;
	
	@ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private PatientEntity patient;
	
	@OneToOne
	@JoinColumn(name="especialidad_id")
	private SpecialtiesEntity especialidad;
	
	private String observacion;
	
	@JsonProperty
	@Column(name = "fecha_solicitud")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechaSolicitud;
	
	@JsonIgnore
	@Column(name = "create_at")
	@Temporal(TemporalType.DATE)
	private Date createAt;
	
	@JsonIgnore
	private boolean estatus;
	
	public OrdenEntity() {}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCantidadCitas() {
		return cantidadCitas;
	}

	public void setCantidadCitas(Integer cantidadCitas) {
		this.cantidadCitas = cantidadCitas;
	}

	public PatientEntity getPatient() {
		return patient;
	}

	public void setPatient(PatientEntity patient) {
		this.patient = patient;
	}

	public SpecialtiesEntity getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(SpecialtiesEntity especialidad) {
		this.especialidad = especialidad;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}
	
	@PrePersist
	public void initializer() {
		this.createAt = new Date();
		this.estatus = true;
	}
}
