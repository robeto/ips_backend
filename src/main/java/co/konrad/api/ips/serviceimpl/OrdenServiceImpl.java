package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.OrdenDAO;
import co.konrad.api.ips.entity.OrdenEntity;
import co.konrad.api.ips.entity.PatientEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.OrdenService;

@Service
public class OrdenServiceImpl implements OrdenService {
	
	@Autowired
	private OrdenDAO ordenDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<OrdenEntity> getOrdenesPaciente(String numberId) {
		return ordenDao.findAllByPatient_numberIdAndEstatusEquals(numberId, true);
	}

	@Override
	@Transactional
	public OrdenEntity createOrden(OrdenEntity orden) {
		return ordenDao.save(orden);
	}
	
	@Override
	@Transactional(readOnly = true)
	public OrdenEntity findById(Long id, String numberId) {
		return ordenDao.finOptionalByIdAndPatient_numberIdAndEstatusEquals(id, numberId, true).orElseThrow(
				() -> new NoFoundException("No hay orden por id: " + id));
	}

	@Override
	public OrdenEntity getById(Long id) {
		return ordenDao.findById(id).orElseThrow(() -> new NoFoundException("No hay orden por id: " + id) );
	}
	
}
