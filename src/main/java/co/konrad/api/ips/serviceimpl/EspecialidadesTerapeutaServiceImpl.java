package co.konrad.api.ips.serviceimpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.EspecialidadesTerapeutaDAO;
import co.konrad.api.ips.entity.EspecialidadesTerapeutaEntity;
import co.konrad.api.ips.entity.SpecialtiesEntity;
import co.konrad.api.ips.entity.TerapeutaEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.EspecialidadesTerapeutaService;

@Service
public class EspecialidadesTerapeutaServiceImpl implements EspecialidadesTerapeutaService {
	
	@Autowired
	private EspecialidadesTerapeutaDAO especialidades;
	
	@Override
	@Transactional
	public EspecialidadesTerapeutaEntity saveSpecialiti(TerapeutaEntity terapeuta, SpecialtiesEntity especialidad) {
		EspecialidadesTerapeutaEntity newEspecialidad = new EspecialidadesTerapeutaEntity(terapeuta, especialidad);
		return especialidades.save(newEspecialidad);
	}

	@Override
	@Transactional(readOnly = true)
	public List<TerapeutaEntity> getTerapeutasBySpecialiti(Long id) {
		List<TerapeutaEntity> terapeutas = new ArrayList<>();
		 especialidades.findAllOptionalByEspecialidad_id(id)
				.stream()
				.forEach(especialidadTerapeuta -> especialidadTerapeuta.forEach(a -> terapeutas.add(a.getTerapeuta())));
		 terapeutas.stream().filter(a -> a.isEstatus());
		 return terapeutas;
	}

	@Override
	@Transactional(readOnly = true)
	public EspecialidadesTerapeutaEntity findByTerpabeutaAndSpecialitie(Long terapeuta, Long especialidad) {
		return especialidades.findOptionalByTerapeuta_idAndEspecialidad_id(terapeuta, especialidad)
				.orElseThrow(() -> new NoFoundException("El terapeuta no tiene esa especialidad."));
	}

	@Override
	public EspecialidadesTerapeutaEntity save(EspecialidadesTerapeutaEntity especialidad) {
		return especialidades.save(especialidad);
	}
	

}
