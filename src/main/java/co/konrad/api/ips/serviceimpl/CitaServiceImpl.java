package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.CitaDAO;
import co.konrad.api.ips.entity.CitaEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.CitaService;

@Service
public class CitaServiceImpl implements CitaService{
	
	@Autowired
	private CitaDAO citaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<CitaEntity> findAll() {
		return (List<CitaEntity>) citaDao.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public CitaEntity findById(Long id) {
		return citaDao.findById(id)
				.orElseThrow(() -> new NoFoundException("No found cita for id: " + id));
	}

	@Override
	@Transactional
	public CitaEntity save(CitaEntity cita) {
		return citaDao.save(cita);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		citaDao.deleteById(id);
	}

}
