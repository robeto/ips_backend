package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.OperatorDAO;
import co.konrad.api.ips.entity.OperatorEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.OperatorService;

@Service
public class OperatorServiceImpl implements OperatorService{
	
	@Autowired
	private OperatorDAO operatorDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<OperatorEntity> findAll() {
		return operatorDao.findALLByEstatusEquals(true);
	}

	@Override
	@Transactional(readOnly = true)
	public OperatorEntity findById(Long id) {
		return operatorDao.findOptionalByIdAndEstatusEquals(id, true)
				.orElseThrow(() -> new NoFoundException("No found Operator for id: " + id));
	}

	@Override
	@Transactional
	public OperatorEntity save(OperatorEntity operator) {
		return operatorDao.save(operator);
	}

}
