package co.konrad.api.ips.serviceimpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.PatientDAO;
import co.konrad.api.ips.entity.PatientEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.PatientService;

@Service
public class PatientServiceImpl implements PatientService {
	
	@Autowired
	private PatientDAO patientDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<PatientEntity> findAll() {
		return patientDao.findALLByEstatusEquals(true);
	}

	@Override
	@Transactional(readOnly = true)
	public PatientEntity findById(Long id) {
		return patientDao.findOptionalByIdAndEstatusEquals(id, true)
				.orElseThrow(() -> new NoFoundException("No found patient for id: " + id));
	}

	@Override
	@Transactional
	public PatientEntity save(PatientEntity patient) {
		return patientDao.save(patient);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		patientDao.deleteById(id);
	}

	@Override
	@Transactional(readOnly = true)
	public PatientEntity findByNumberId(String numberid) {
		return patientDao.findByNumberIdAndEstatusEquals(numberid, true);
	}


}
