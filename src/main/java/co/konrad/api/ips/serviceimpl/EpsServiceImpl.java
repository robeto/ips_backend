package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.konrad.api.ips.DAO.EpsDAO;
import co.konrad.api.ips.entity.EpsEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.EpsService;

@Service
public class EpsServiceImpl implements EpsService {
	
	@Autowired
	private EpsDAO epsDao;
	
	@Override
	public List<EpsEntity> getAll() {
		return epsDao.findALLByStatusEquals(true);
	}

	@Override
	public EpsEntity getEps(Long id) {
		return epsDao.findOptionalByIdAndStatusEquals(id, true).orElseThrow(
				() -> new NoFoundException("No found EPS for id: " + id));
	}

	@Override
	public EpsEntity newEps(EpsEntity eps) {
		return epsDao.save(eps);
	}

}
