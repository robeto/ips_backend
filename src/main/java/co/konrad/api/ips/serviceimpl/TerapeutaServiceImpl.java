package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.TerapeutaDAO;
import co.konrad.api.ips.entity.TerapeutaEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.TerapeutaService;

@Service
public class TerapeutaServiceImpl implements TerapeutaService {
	
	@Autowired
	private TerapeutaDAO terapeutaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<TerapeutaEntity> findAll() {
		return  terapeutaDao.findALLByEstatusEquals(true);
	}

	@Override
	@Transactional(readOnly = true)
	public TerapeutaEntity findById(Long id) {
		return terapeutaDao.findOptionalByIdAndEstatusEquals(id, true)
				.orElseThrow(() -> new NoFoundException("No found Terapeuta for id: " + id));
	}

	@Override
	@Transactional
	public TerapeutaEntity save(TerapeutaEntity terapeuta) {
		return terapeutaDao.save(terapeuta);
	}

}
