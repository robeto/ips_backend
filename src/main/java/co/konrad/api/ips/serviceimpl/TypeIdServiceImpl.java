package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.TypeIdDAO;
import co.konrad.api.ips.entity.TypeIdEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.TypeIdService;

@Service
public class TypeIdServiceImpl implements TypeIdService {

	@Autowired
	private TypeIdDAO typeIdDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<TypeIdEntity> getAll() {
		return typeIdDao.findALLByStatusEquals(true);
	}

	@Override
	@Transactional(readOnly = true)
	public TypeIdEntity getTypeId(Long id) {
		return typeIdDao.findOptionalByIdAndStatusEquals(id, true).orElseThrow(
				() -> new NoFoundException("No found Terapeuta for id: " + id));
	}

	@Override
	@Transactional
	public TypeIdEntity newTypeId(TypeIdEntity typeId) {
		return typeIdDao.save(typeId);
	}

}
