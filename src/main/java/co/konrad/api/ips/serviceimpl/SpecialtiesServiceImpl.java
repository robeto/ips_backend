package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.SpecialtiesDAO;
import co.konrad.api.ips.entity.SpecialtiesEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.SpecialtiesService;

@Service
public class SpecialtiesServiceImpl implements SpecialtiesService {
	
	@Autowired
	private SpecialtiesDAO specialtiesDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<SpecialtiesEntity> getAll() {
		return specialtiesDao.findAllByStatusEquals(true);
	}

	@Override
	@Transactional(readOnly = true)
	public SpecialtiesEntity getById(Long id) {
		return specialtiesDao.findOptionalByIdAndStatusEquals(id, true).orElseThrow(
				() -> new NoFoundException("No se encuentra especialidad con ID: " +id));
	}

	@Override
	@Transactional
	public SpecialtiesEntity save(SpecialtiesEntity newSpecialties) {
		return specialtiesDao.save(newSpecialties);
	}

	@Override
	@Transactional
	public void deleteById(SpecialtiesEntity specialties) {
		specialtiesDao.save(specialties);
	}

}
