package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.ArlDAO;
import co.konrad.api.ips.entity.ArlEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.ArlService;

@Service
public class ArlServiceImpl implements ArlService {
	
	@Autowired
	private ArlDAO arlDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<ArlEntity> getAll() {
		return arlDao.findALLByStatusEquals(true);
	}

	@Override
	@Transactional(readOnly = true)
	public ArlEntity getArl(Long id) {
		return arlDao.findOptionalByIdAndStatusEquals(id, true).orElseThrow(
				() -> new NoFoundException("No found Terapeuta for id: " + id));
	}

	@Override
	@Transactional
	public ArlEntity newArl(ArlEntity arl) {
		return arlDao.save(arl);
	}

}
