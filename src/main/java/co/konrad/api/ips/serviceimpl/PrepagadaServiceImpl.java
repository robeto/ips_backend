package co.konrad.api.ips.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.konrad.api.ips.DAO.PrepagadaDAO;
import co.konrad.api.ips.entity.PrepagadaEntity;
import co.konrad.api.ips.exception.NoFoundException;
import co.konrad.api.ips.service.PrepagadaService;

@Service
public class PrepagadaServiceImpl implements PrepagadaService {
	
	@Autowired
	private PrepagadaDAO prepagadaDao;
	
	@Override
	@Transactional(readOnly = true)
	public List<PrepagadaEntity> getAll() {
		return prepagadaDao.findALLByStatusEquals(true);
	}

	@Override
	@Transactional(readOnly = true)
	public PrepagadaEntity getPrepagada(Long id) {
		return prepagadaDao.findOptionalByIdAndStatusEquals(id, true).orElseThrow(
				() -> new NoFoundException("No found Prepagada for id: " + id));
	}

	@Override
	public PrepagadaEntity newPrepagada(PrepagadaEntity prepagada) {
		return prepagadaDao.save(prepagada);
	}

}
