package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.ArlEntity;

public interface ArlService {
	
	public List<ArlEntity> getAll();
	
	public ArlEntity getArl(Long id);
	
	public ArlEntity newArl(ArlEntity typeId);
}
