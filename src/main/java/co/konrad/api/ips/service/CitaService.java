package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.CitaEntity;


public interface CitaService {
	
	public List<CitaEntity>findAll();
	
	public CitaEntity findById(Long id);
	
	public CitaEntity save(CitaEntity cita);
	
	public void delete(Long id);
	
}
