package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.TypeIdEntity;

public interface TypeIdService {
	
	public List<TypeIdEntity> getAll();
	
	public TypeIdEntity getTypeId(Long id);
	
	public TypeIdEntity newTypeId(TypeIdEntity typeId);
	
}
