package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.EpsEntity;

public interface EpsService {
		
	public List<EpsEntity> getAll();
	
	public EpsEntity getEps(Long id);
	
	public EpsEntity newEps(EpsEntity eps);
	
}
