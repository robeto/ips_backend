package co.konrad.api.ips.service;

import java.util.List;
import co.konrad.api.ips.entity.PrepagadaEntity;

public interface PrepagadaService {
	
	public List<PrepagadaEntity> getAll();
	
	public PrepagadaEntity getPrepagada(Long id);
	
	public PrepagadaEntity newPrepagada(PrepagadaEntity prepagada);
}
