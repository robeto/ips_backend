package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.OperatorEntity;

public interface OperatorService {
	
	public List<OperatorEntity>findAll();
	
	public OperatorEntity findById(Long id);
	
	public OperatorEntity save(OperatorEntity patient);
	
}
