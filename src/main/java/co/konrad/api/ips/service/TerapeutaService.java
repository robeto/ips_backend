package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.TerapeutaEntity;

public interface TerapeutaService {
	
	public List<TerapeutaEntity>findAll();
	
	public TerapeutaEntity findById(Long id);
	
	public TerapeutaEntity save(TerapeutaEntity terapeuta);
	
}
