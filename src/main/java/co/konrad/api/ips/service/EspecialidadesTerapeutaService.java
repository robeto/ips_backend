package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.EspecialidadesTerapeutaEntity;
import co.konrad.api.ips.entity.SpecialtiesEntity;
import co.konrad.api.ips.entity.TerapeutaEntity;

public interface EspecialidadesTerapeutaService {
	
	public EspecialidadesTerapeutaEntity saveSpecialiti(TerapeutaEntity terapeuta, SpecialtiesEntity especialidad);
	
	public List<TerapeutaEntity> getTerapeutasBySpecialiti(Long id);
	
	public EspecialidadesTerapeutaEntity findByTerpabeutaAndSpecialitie(Long terapeuta, Long especialidad);
	
	public EspecialidadesTerapeutaEntity save(EspecialidadesTerapeutaEntity especialidad);
	
}
