package co.konrad.api.ips.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import co.konrad.api.ips.entity.PatientEntity;

public interface PatientService {
	
	public List<PatientEntity>findAll();
	
	public PatientEntity findById(Long id);
	
	public PatientEntity findByNumberId(String numberid);
	
	public void delete(Long id);
	
	public PatientEntity save(PatientEntity patient);
}
