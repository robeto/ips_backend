package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.OrdenEntity;
import co.konrad.api.ips.entity.PatientEntity;

public interface OrdenService {
	
	public List<OrdenEntity> getOrdenesPaciente( String numberId);
	
	public OrdenEntity createOrden(OrdenEntity orden);
	
	public OrdenEntity findById(Long id, String numberId);
	
	public OrdenEntity getById(Long id);
}
