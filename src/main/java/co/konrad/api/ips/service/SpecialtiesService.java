package co.konrad.api.ips.service;

import java.util.List;

import co.konrad.api.ips.entity.SpecialtiesEntity;

public interface SpecialtiesService {
	
	public List<SpecialtiesEntity> getAll();
	
	public SpecialtiesEntity getById(Long Id);
	
	public SpecialtiesEntity save(SpecialtiesEntity newSpecialties);
	
	public void deleteById(SpecialtiesEntity specialties);
}
