package co.konrad.api.ips.DTO;

public class EspeacilidadTerapeutaRequestDto {
	
	private Long idTerapeuta;
	
	private Long idEspecialidad;

	public Long getIdTerapeuta() {
		return idTerapeuta;
	}

	public void setIdTerapeuta(Long idTerapeuta) {
		this.idTerapeuta = idTerapeuta;
	}

	public Long getIdEspecialidad() {
		return idEspecialidad;
	}

	public void setIdEspecialidad(Long idEspecialidad) {
		this.idEspecialidad = idEspecialidad;
	}
	
	
	
	

}
