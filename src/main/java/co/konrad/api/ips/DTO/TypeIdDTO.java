package co.konrad.api.ips.DTO;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class TypeIdDTO {

		private Long id;
		
		private String name;
		
		@JsonIgnore
		private Date createAt;
		
		@JsonIgnore
		private boolean status;
		
		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Date getCreateAt() {
			return createAt;
		}

		public void setCreateAt(Date createAt) {
			this.createAt = createAt;
		}

		public boolean isStatus() {
			return status;
		}

		public void setStatus(boolean status) {
			this.status = status;
		}
		
}
