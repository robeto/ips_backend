package co.konrad.api.ips.DTO;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import co.konrad.api.ips.entity.TypeIdEntity;
import io.swagger.annotations.ApiModelProperty;

public class OperatorDTO implements Serializable{
	
private static final long serialVersionUID = 1L;
	
	private Long id;
	
	@JsonProperty
	@ApiModelProperty(example = "1", 
	required = true)
	private TypeIdEntity typeId;
	
	@ApiModelProperty(example = "1016985332", required = true)
	@JsonProperty
	private String numberId;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private String lastName;
	
	@ApiModelProperty(example = "6329586", required = true)
	@JsonProperty
	private String phone;
	
	@ApiModelProperty(example = "3015896326", required = true)
	@JsonProperty
	private String cellPhone;
	
	@ApiModelProperty(example = "Cll falsa 123", required = true)
	@JsonProperty
	private String address;
	
	@JsonIgnore
	private Date createAt;
	
	@JsonIgnore
	private boolean estatus;
	
	public OperatorDTO() {}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeIdEntity getTypeId() {
		return typeId;
	}

	public void setTypeId(TypeIdEntity typeId) {
		this.typeId = typeId;
	}

	public String getNumberId() {
		return numberId;
	}

	public void setNumberId(String numberId) {
		this.numberId = numberId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	
	
	
}
