package co.konrad.api.ips.DTO;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import co.konrad.api.ips.entity.ArlEntity;
import co.konrad.api.ips.entity.EpsEntity;
import co.konrad.api.ips.entity.PrepagadaEntity;
import co.konrad.api.ips.entity.TypeIdEntity;
import io.swagger.annotations.ApiModelProperty;

public class PatientDTO {
	
	private Long id;
	
	@JsonProperty
	@ApiModelProperty(example = "1", 
	required = true)
	private TypeIdEntity typeId;
	
	@ApiModelProperty(example = "1016985332", required = true)
	@JsonProperty
	private String numberId;
	
	@JsonProperty
	private String name;
	
	@JsonProperty
	private String lastName;
	
	@ApiModelProperty(example = "6329586", required = true)
	@JsonProperty
	private String phone;
	
	@ApiModelProperty(example = "3015896326", required = true)
	@JsonProperty
	private String cellPhone;
	
	@ApiModelProperty(example = "Cll falsa 123", required = true)
	@JsonProperty
	private String address;
	
	@JsonIgnore
	private EpsEntity eps;
	
	@JsonIgnore
	private Date fechaNacimiento;
	
	@JsonIgnore
	private boolean isPrepagada;
	
	@JsonIgnore
	private PrepagadaEntity prepagada;
	
	@JsonIgnore
	private ArlEntity arl;
	
	@JsonIgnore
	private boolean isChild;
	
	@JsonIgnore
	private boolean isOld;
	
	private String acudiente;
	
	@ApiModelProperty(example = "3015896326", required = true)
	@JsonProperty
	private String phoneAcudiente;
	
	@JsonIgnore
	private Date createAt;
	
	@JsonIgnore
	private boolean estatus;
	
	public PatientDTO() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public TypeIdEntity getTypeId() {
		return typeId;
	}

	public void setTypeId(TypeIdEntity typeId) {
		this.typeId = typeId;
	}

	public String getNumberId() {
		return numberId;
	}

	public void setNumberId(String numberId) {
		this.numberId = numberId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCellPhone() {
		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {
		this.cellPhone = cellPhone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public EpsEntity getEps() {
		return eps;
	}

	public void setEps(EpsEntity eps) {
		this.eps = eps;
	}

	public boolean isPrepagada() {
		return isPrepagada;
	}

	public void setPrepagada(boolean isPrepagada) {
		this.isPrepagada = isPrepagada;
	}

	public boolean isChild() {
		return isChild;
	}

	public void setChild(boolean isChild) {
		this.isChild = isChild;
	}

	public boolean isOld() {
		return isOld;
	}

	public void setOld(boolean isOld) {
		this.isOld = isOld;
	}

	public String getAcudiente() {
		return acudiente;
	}

	public void setAcudiente(String acudiente) {
		this.acudiente = acudiente;
	}

	public String getPhoneAcudiente() {
		return phoneAcudiente;
	}

	public void setPhoneAcudiente(String phoneAcudiente) {
		this.phoneAcudiente = phoneAcudiente;
	}

	public Date getCreateAt() {
		return createAt;
	}
	

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public PrepagadaEntity getPrepagada() {
		return prepagada;
	}

	public void setPrepagada(PrepagadaEntity prepagada) {
		this.prepagada = prepagada;
	}

	public ArlEntity getArl() {
		return arl;
	}

	public void setArl(ArlEntity arl) {
		this.arl = arl;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}
}
