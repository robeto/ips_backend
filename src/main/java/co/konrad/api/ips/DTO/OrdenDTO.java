package co.konrad.api.ips.DTO;

import java.util.Date;

import co.konrad.api.ips.entity.PatientEntity;
import co.konrad.api.ips.entity.SpecialtiesEntity;

public class OrdenDTO {
	
	private Long id;
	
	private Integer cantidadCitas;
	
	private PatientEntity patient;
	
	private SpecialtiesEntity especialidad;
	
	private String observacion;
	
	private Date fechaSolicitud;
	
	private Date createAt;
	
	private boolean estatus;
	
	public OrdenDTO() {}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getCantidadCitas() {
		return cantidadCitas;
	}

	public void setCantidadCitas(Integer cantidadCitas) {
		this.cantidadCitas = cantidadCitas;
	}

	public PatientEntity getPatient() {
		return patient;
	}

	public void setPatient(PatientEntity patient) {
		this.patient = patient;
	}

	public SpecialtiesEntity getEspecialidad() {
		return especialidad;
	}

	public void setEspecialidad(SpecialtiesEntity especialidad) {
		this.especialidad = especialidad;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}
	
	public Date getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(Date fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public boolean isEstatus() {
		return estatus;
	}

	public void setEstatus(boolean estatus) {
		this.estatus = estatus;
	}
	
}
