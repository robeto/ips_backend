package co.konrad.api.ips.DTO;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

import co.konrad.api.ips.entity.OrdenEntity;
import co.konrad.api.ips.entity.PatientEntity;
import co.konrad.api.ips.entity.TerapeutaEntity;

public class CitaDTO {
	
	private Long id;
	
	private Date horarioTerapeuta;
	
	private TerapeutaEntity terapeutas;

	private String zona;
	
	private String createAt;
	
	private String evolucion;

	private String observaciones;
	
	private String pago;
	
	private OrdenEntity orden;
	
	public CitaDTO() {
	
	}

	public CitaDTO(Long id, Date horarioTerapeuta, TerapeutaEntity terapeutas,
			String zona, String createAt, String evolucion, String observaciones,
			String pago, OrdenEntity orden) {
		super();
		this.id = id;
		this.horarioTerapeuta = horarioTerapeuta;
		this.terapeutas = terapeutas;
		this.zona = zona;
		this.createAt = createAt;
		this.evolucion = evolucion;
		this.observaciones = observaciones;
		this.pago = pago;
		this.orden = orden;
	}
	
	
	
	public OrdenEntity getOrden() {
		return orden;
	}

	public void setOrden(OrdenEntity orden) {
		this.orden = orden;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getHorarioTerapeuta() {
		return horarioTerapeuta;
	}

	public void setHorarioTerapeuta(Date horarioTerapeuta) {
		this.horarioTerapeuta = horarioTerapeuta;
	}

	public String getZona() {
		return zona;
	}

	public void setZona(String zona) {
		this.zona = zona;
	}

	public String getCreateAt() {
		return createAt;
	}

	public void setCreateAt(String createAt) {
		this.createAt = createAt;
	}

	public String getEvolucion() {
		return evolucion;
	}

	public void setEvolucion(String evolucion) {
		this.evolucion = evolucion;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public String getPago() {
		return pago;
	}

	public void setPago(String pago) {
		this.pago = pago;
	}

	public TerapeutaEntity getTerapeutas() {
		return terapeutas;
	}

	public void setTerapeutas(TerapeutaEntity terapeutas) {
		this.terapeutas = terapeutas;
	}
	
}
