package co.konrad.api.ips.DTO;

public class OrdenRequestDto {
	
	private Long idOrden;
	
	private String numberId;

	public Long getIdOrden() {
		return idOrden;
	}

	public void setIdOrden(Long idOrden) {
		this.idOrden = idOrden;
	}

	public String getNumberId() {
		return numberId;
	}

	public void setNumberId(String numberId) {
		this.numberId = numberId;
	}

}
