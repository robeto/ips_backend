> # *IPS_BACKEND*
>
> This project stores the backend made in spring. The following are the tools used for development.This project stores the backend made in 
> spring. The following are the tools used for development.

> ## Tools
> The following development tools were used:
>
> - IDE Spring tools 4 [Download](https://spring.io/tools)
> - JDK 1.8 Oracle SE 8u221 [Download](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
> -   - Installation instructions JDK [Reference link](https://platzi.com/tutoriales/1222-java-basico/201-instalar-java-en-windows-y-configurar-variables-de-entorno/)
> - Gradle 5.5.1 Download binary-only [Download](https://gradle.org/releases/)
>     - Installation instructions Gradle [Reference link](https://gradle.org/install/)
> - MySQL 8.0.17 [Download](https://dev.mysql.com/downloads/mysql/)
>     - go to the bottom of the page and download ZIP archive.
>     - Create conection to localhost port 3307
>     - Create data base
>         - name -> db_ips
>         - charset/collation -> utf8 utf8_bin 
>     - Create user with all permiss
>         - name     -> dev
>         - password -> Password2019