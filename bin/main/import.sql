/* Type ids */
INSERT INTO type_id(id, name, status, create_at) values(1, "Cedula", 1,  "2019-06-01");
INSERT INTO type_id(id, name, status, create_at) values(2, "Tarjeta de identidad", 1,  "2019-06-01");
INSERT INTO type_id(id, name, status, create_at) values(3, "Cedula de extrangeria", 1,  "2019-06-01");
INSERT INTO type_id(id, name, status, create_at) values(4, "Carnet diplomatico", 1, "2019-06-01");

/* especialidades */
INSERT INTO specialties(id, name, status, create_at) values(1,"Fisioterapia", 1, "2019-06-01");
INSERT INTO specialties(id, name, status, create_at) values(2,"Fisiatría", 1, "2019-06-01");
INSERT INTO specialties(id, name, status, create_at) values(3,"Psicología", 1, "2019-06-01");
INSERT INTO specialties(id, name, status, create_at) values(4,"Terapia ocupacional", 1, "2019-06-01");
INSERT INTO specialties(id, name, status, create_at) values(5,"Neuropsicología", 1, "2019-06-01");
INSERT INTO specialties(id, name, status, create_at) values(6,"Fonoaudiología", 1, "2019-06-01");
INSERT INTO specialties(id, name, status, create_at) values(7,"Inyectología", 1, "2019-06-01");
